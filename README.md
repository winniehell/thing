# Thing

## How do I run this thing?

### Prerequisites

- You should have [`Yarn`](https://classic.yarnpkg.com/en/) installed
- A recent version of [`Node`](https://nodejs.org/en/)

### Setup

```
yarn
yarn build
```

Then open up the `public/index.html` in your favorite browser :)

```
firefox public/index.html
```
