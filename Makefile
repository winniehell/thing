out_dir = public
out_bundle_js = ${out_dir}/bundle.js
out_index_html = ${out_dir}/index.html

build_dir = build
build_inject = ${build_dir}/inject.js
build_rollup = ${build_dir}/rollup.config.js
build_shim = ${build_dir}/*.mustache

src_dir = src
src_files = ${src_dir}/* ${src_dir}/**/*

node_modules_dir = node_modules
node_modules_files = ${node_modules_dir}/*

.PHONY: all clean

all: ${out_bundle_js} ${out_index_html}

clean:
	yarn run clean

${out_bundle_js}: ${src_files} ${node_modules_files} ${build_rollup}
	mkdir -p ${out_dir}
	yarn run build:bundle
	stat ${out_bundle_js}

${out_index_html}: ${out_bundle_js} ${build_inject} ${build_shim}
	yarn run build:main
