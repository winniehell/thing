const BASE_WIDTH = 6;
const BASE_PATH = new Path2D("M 0 30 L 0 3 Q 3 0, 6 0 L 6 30 Z");

const drawBaseShape = (x, y) => {
  c.save();
  c.translate(x, y);
  c.fill(BASE_PATH);
  c.stroke(BASE_PATH);
  c.restore();
};

export const drawGrass = () => {
  c.save();
  c.lineWidth = 1;
  c.strokeStyle = "green";
  c.fillStyle = "lightgreen";

  for (let i = 0; i < a.width / BASE_WIDTH; i++) {
    const height = 20 + Math.sin(i ** 3 % 10) ** 2 * 10;
    drawBaseShape(i * BASE_WIDTH, a.height - height);
  }
  c.restore();
};

export const drawClouds = () => {
  c.lineWidth = 6;
  c.strokeStyle = "transparent";
  c.fillStyle = "white";

  c.shadowColor = "lightblue";
  c.shadowBlur = 15;

  c.save();
  c.rotate(-Math.PI / 2, a.clientWidth / 2, a.clientHeight / 2);
  drawBaseShape(-100, 100, 50, 300);
  c.rotate(Math.PI, a.clientWidth / 2, a.clientHeight / 2);
  drawBaseShape(0, -400, 50, 300);
  c.restore();
};
