/* global a: Canvas */
/* global b */
/* global c: Canvas.getContext('2d') */
/* global d */
import { drawGrass } from "./graphics/index";
import startAnimation from "./animate";
import "./music/index";

const loop = (dt, t) => {
  a.width += 0;
  for (let i = 0; i < 8; i++) {
    c.fillRect(200 + 100 * i + 40 * Math.sin(t), 200, 80, 160);
  }
  drawGrass();
};

startAnimation(loop);
