/**
 * NOTES:
 * - We do t/1e3 so that our time elapsed is in seconds
 */
export default function startAnimation(loop, lastTime = 0) {
  let animate = t => {
    loop(lastTime ? t / 1e3 - lastTime : 0, (lastTime = t / 1e3));
    requestAnimationFrame(animate);
  };
  animate(0);
}
