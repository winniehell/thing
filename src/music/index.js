const audioCtx = new AudioContext();

const output = audioCtx.createGain();
output.gain.setValueAtTime(0.01, audioCtx.currentTime);
output.connect(audioCtx.destination);

const keyNumToFrequency = i => 440 * Math.pow(2, (i - 49) / 12);

const play = (instrument, noteKeys, noteLengthInSeconds) => {
  noteKeys
    .map(keyNumToFrequency)
    .concat(0)
    .forEach((f, i) =>
      instrument(f, audioCtx.currentTime + i * noteLengthInSeconds)
    );
};

const applyPattern = (values, pattern) =>
  values.reduce((a, i) => a.concat(pattern(i)), []);

const tripletPattern = b => [b, b + 4, b + 7];
const melodyPattern = b => applyPattern([b, b, b + 2, b + 4], tripletPattern);

const createMelodyInstrument = () => {
  const low = audioCtx.createOscillator();
  low.type = "sine";
  low.connect(output);
  low.start();

  const gain = audioCtx.createGain();
  gain.connect(output);

  const gainValue = audioCtx.createOscillator();
  gainValue.type = "sine";
  gainValue.frequency.value = 1 / 0.3;
  gainValue.connect(gain.gain);
  gainValue.start();

  const high = audioCtx.createOscillator();
  high.type = "sine";
  high.connect(gain);
  high.start();

  return (f, t) => {
    low.frequency.setValueAtTime(f, t);
    high.frequency.setValueAtTime(f * 2, t);
  };
};

play(
  createMelodyInstrument(),
  [
    ...melodyPattern(42),
    ...melodyPattern(42),
    ...melodyPattern(40).reverse(),
    ...melodyPattern(38)
  ],
  0.3
);

const createBassInstrument = () => {
  const oscillatorNode = audioCtx.createOscillator();
  oscillatorNode.type = "triangle";
  oscillatorNode.connect(output);
  oscillatorNode.start();

  return (f, t) => oscillatorNode.frequency.setValueAtTime(f, t);
};

play(
  createBassInstrument(),
  [...tripletPattern(18), ...tripletPattern(20).reverse(), 18, 20],
  1.8
);
