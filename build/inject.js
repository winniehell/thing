const argv = require("yargs").argv;
const fs = require("fs");
const Mustache = require("mustache");

const promisify = fn => (...args) =>
  new Promise((resolve, reject) => {
    fn(...args, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });

const readFileAsync = promisify((path, cb) => fs.readFile(path, "utf8", cb));
const writeFileAsync = promisify((path, data, cb) =>
  fs.writeFile(path, data, "utf8", cb)
);

function fail(msg) {
  console.error(msg);
  process.exit(1);
}

function main({
  source: sourcePath,
  template: templatePath,
  output: outputPath
}) {
  if (!sourcePath) fail("Please specify a --source");
  if (!templatePath) fail("Please specify a --template");
  if (!outputPath) fail("Please specify a --output");

  return Promise.all([
    readFileAsync(sourcePath),
    readFileAsync(templatePath)
  ]).then(([source, template]) => {
    const output = Mustache.render(template, { source });

    return writeFileAsync(outputPath, output);
  });
}

main(argv);
