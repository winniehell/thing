const { terser } = require("rollup-plugin-terser");

const config = {
  input: "src/index.js",
  output: {
    file: "public/bundle.js",
    format: "iife",
    strict: false
  },
  plugins: [
    terser({
      compress: {
        unsafe: true,
        warnings: true
      }
    })
  ]
};

export default config;
